$(document).ready(function(){
	console.log("Funcionando. . .");
	// $('h1').html('Cursando JQuery junto con GitLab')
	// $('.display-4').html('desde la clase de html')
	//$('#id').html()
	//$('.clase h1').html('cambio de texto')
	//$('.clase h1:first').html('cambio de texto')
	//$('.clase h1:last').html('cambio de texto')

	$('#idh1').addClass('text-danger')
	$('#idh1').removeClass('display-4')

	$('#contenido').append('<h1>Este es un elemento  al final</h1>');
	$('#contenido').prepend('<h1>Este es un elemento  al principio</h1>');

	$('#color-azul').css('color','blue');
	$('#color-azul').css({color:'blue',background:'salmon',padding:'28px'});

	//$('#color-azul').remove();
	$('#color-azul').hide();

	$('img').attr('src','https://miro.medium.com/max/434/1*AQoZ21EIDpQpKk1hSvE1cA.png');

	var parrafo = $('#resultado p');
	$('.btn-primary').click(function(){
		parrafo.addClass('display-4');
	})

	$('.btn-danger').click(function(){
		parrafo.removeClass('display-4');
	})

	$('.btn-warning').click(function(){
		parrafo.toggleClass('display-4');
	})

	$('#formulario').submit(function(evento){
		evento.preventDefault();
		var nombre = $('#nombre').val();
		console.log(nombre);
	})


	var resultado = $('#result');
	$('#add').click(function(){
		resultado.hide(1000);
	})

	$('#delete').click(function(){
		resultado.show();
	})

	$('#ambos').click(function(){
		resultado.toggle(1000);
	})

})
